﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FoodCharacter : MonoBehaviour
{
	public int _characterNumber;
	public string _characterName;
	public string _twitterName;
	public string _twitterUserID;
	public string _twitterUserToken;
	public string _twitterUserSecretToken;

	public FoodCharacter(int characterNumber, string characterName, string twitterName, string twitterUserID, string twitterUserToken, string twitterUserSecretToken)
	{
		_characterNumber = characterNumber;
		_characterName = characterName;
		_twitterName = twitterName;
		_twitterUserID = twitterUserID;
		_twitterUserToken = twitterUserToken;
		_twitterUserSecretToken = twitterUserSecretToken;
	}

	public int CharacterNumber
	{
		get
		{
			return _characterNumber;
		}
		private set
		{
			_characterNumber = value;
		}
	}

	public string CharacterName
	{
		get
		{
			return _characterName;
		}
		private set
		{
			_characterName = value;
		}
	}
	public string TwitterName
	{
		get
		{
			return _twitterName;
		}
		private set
		{
			_twitterName = value;
		}
	}
	public string TwitterUserID
	{
		get
		{
			return _twitterUserID;
		}
		private set
		{
			_twitterUserID = value;
		}
	}
	public string TwitterUserToken
	{
		get
		{
			return _twitterUserToken;
		}
		private set
		{
			_twitterUserToken = value;
		}
	}
	public string TwitterUserSecretToken
	{
		get
		{
			return _twitterUserSecretToken;
		}
		private set
		{
			_twitterUserSecretToken = value;
		}
	}

}
