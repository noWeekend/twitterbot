﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {


	public float timeBetweenChecks = 300;
	public float timer = 0;
	private TwitterFunctions twitterFunctions;
	public bool isPaused = true;

	private LogManager logManager;

	public List<FoodCharacter> characterList = new List<FoodCharacter>();
	public DatabaseConnect databaseConnect;

	// Use this for initialization
	void Awake () {
		twitterFunctions = GetComponent<TwitterFunctions>();
		logManager = GetComponent<LogManager>();
		timer = timeBetweenChecks;
		twitterFunctions.GetLastPlayerID(null);
		databaseConnect = GetComponent<DatabaseConnect>();
	}

	public void SetCharacters(string rawCharacterString)
	{
		string[] characterString = rawCharacterString.Split(new[] { "\r\n", "\r", "\n" }, System.StringSplitOptions.None);

		foreach (string attributes in characterString)
		{
			if (string.IsNullOrEmpty(attributes))
			{
				continue;
			}
			string[] attributeArray = attributes.Split('\t');
			
			FoodCharacter newCharacter = new FoodCharacter(int.Parse(attributeArray[0]), attributeArray[1], attributeArray[2], attributeArray[3], attributeArray[4], attributeArray[5]);
			characterList.Add(newCharacter);
		}
	
	}

	public FoodCharacter GetCharacter(int characterNumber)
	{
		foreach (FoodCharacter character in characterList)
		{
			if (character.CharacterNumber == characterNumber)
			{
				return character;
			}
		}
		logManager.AddLog("Could Not Find Character Number: " + characterNumber);

		return null;
	}

	public List<Dropdown.OptionData> GetFullCharacterList()
	{
		List<Dropdown.OptionData> charList = new List<Dropdown.OptionData>();
		foreach (FoodCharacter character in characterList)
		{
			Dropdown.OptionData dataItem = new Dropdown.OptionData(character.CharacterName);
			charList.Add(dataItem);
		}

		return charList;
	}

	// Update is called once per frame
	void Update () {

		if (isPaused)
		{
			return;
		}

		timer += Time.deltaTime;

		if (timer >= timeBetweenChecks)
		{
			timer = 0;
			twitterFunctions.SearchTwitterForNewAccounts();
			logManager.AddLog("Scanning for new players");
		}
	}

	private void UnPause()
	{
		isPaused = false;
	}

	private void Pause()
	{
		isPaused = true;
	}
}
