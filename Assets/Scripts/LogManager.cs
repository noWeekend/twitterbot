﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogManager : MonoBehaviour {

	public string logFile;
	public Text logOutputText;
	public int logLinesMax;
	public List<string> logFileList = new List<string>();

	void AddToOnScreenOutput(string logLine)
	{
		logFileList.Add(logLine);

		if (logFileList.Count > logLinesMax)
		{
			logFileList.RemoveAt(0);
		}

		string outputString = "";
		for (int i = logFileList.Count-1; i >= 0; i--)
		{
			outputString += logFileList[i] + "\n";
		}

		logOutputText.text = outputString;
	}

	public void AddLog(string log,bool isError = false)
	{
		AddToOnScreenOutput(System.DateTime.Now.ToString() + " - " + log);

		if (isError) {
			Debug.LogError(log);
		} else {
			Debug.Log(log);
		}
	}
}
