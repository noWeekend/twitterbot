﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwitterAccount {

	public long Id { get; set; }
	public string ScreenName { get; set; }
	public string Name { get; set; }
	public string Description { get; set; }
	public string ImageUrl { get; set; }

	public TwitterAccount(long id, string screenName, string name, string description, string imageUrl)
	{
		Id = id;
		ScreenName = screenName;
		Name = name;
		Description = description;
		ImageUrl = imageUrl;
	}
}
