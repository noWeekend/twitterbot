﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TokenLibraryItem
{
	public string twitterScreenName = "TwitterUserScreenName";
	public string twitterUserID = "TwitterUserID";
	public string twitterUserToken = "TwitterUserToken";
	public string twitterUserTokenSecret = "TwitterUserTokenSecret";

	public TokenLibraryItem(string userId, string screenName, string userToken, string userTokenSecret)
	{
		twitterUserID = userId;
		twitterScreenName = screenName;
		twitterUserToken = userToken;
		twitterUserTokenSecret = userTokenSecret;
	}

	public void UpdateDetails(string userId, string userToken, string userTokenSecret)
	{
		twitterUserID = userId;
		twitterUserToken = userToken;
		twitterUserTokenSecret = userTokenSecret;
	}

	public string ScreenName
	{
		get { return twitterScreenName; }
	}

	public string UserID
	{
		get { return twitterUserID; }
	}

	public string UserToken
	{
		get { return twitterUserToken; }
	}

	public string UserTokenSecret
	{
		get { return twitterUserTokenSecret; }
	}

	public TwitterOther.RequestTokenResponse RequestTokenResponse()
	{
		return new TwitterOther.RequestTokenResponse(twitterUserToken, twitterUserTokenSecret);
	}

	public TwitterOther.AccessTokenResponse AccessTokenResponse()
	{
		return new TwitterOther.AccessTokenResponse(twitterUserToken, twitterUserTokenSecret, twitterUserID,twitterScreenName);
	}

	public string SavingString()
	{
		return twitterUserID + "," + twitterScreenName + "," + twitterUserToken + "," + twitterUserTokenSecret;
	}

}
