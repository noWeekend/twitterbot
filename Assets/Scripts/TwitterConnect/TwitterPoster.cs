
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.IO;

public class TwitterPoster : MonoBehaviour 
{

	public List<TokenLibraryItem> tokenLibrary = new List<TokenLibraryItem>();

	public InputField pinInputField;
	public InputField tweetInputfield;
	public Button pinEnterButton;
	public Button tweetButton;
	public Button RegisterAccountButton;
	public Text infoText;
	public InputField userInputField;

    // You need to register your game or application in Twitter to get cosumer key and secret.
    // Go to this page for registration: http://dev.twitter.com/apps/new
    public string CONSUMER_KEY;
    public string CONSUMER_SECRET;

 //   public InputField hashtag;
 //   public InputField hashtagNum;

	//// You need to save access token and secret for later use.
	//// You can keep using them whenever you need to access the user's Twitter account. 
	//// They will be always valid until the user revokes the access to your application.
	//public string PLAYER_PREFS_TWITTER_USER_ID           = "TwitterUserID";
	//public string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME  = "TwitterUserScreenName";
	//public string PLAYER_PREFS_TWITTER_USER_TOKEN        = "TwitterUserToken";
	//public string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";

    TwitterOther.RequestTokenResponse m_RequestTokenResponse;
	TwitterOther.AccessTokenResponse m_AccessTokenResponse;

    string m_PIN = "Please enter your PIN here.";
    string m_Tweet = "Please enter your tweet here.";

	// Use this for initialization
	void Start() 
    {
		LoadTwitterTokenLibrary();
	}


	string tolkenLibraryPath = "Assets/Resources/AccessTokens.csv";

	void LoadTwitterTokenLibrary()
	{
		string results = File.ReadAllText(tolkenLibraryPath);

		string[] tolkenGroups = results.Split('\n');

		foreach (string tolkenGroup in tolkenGroups)
		{
			if (tolkenGroup == "")
			{
				continue;
			}

			string[] tolkenItems = tolkenGroup.Split(',');
			tokenLibrary.Add(new TokenLibraryItem(tolkenItems[0], tolkenItems[1], tolkenItems[2], tolkenItems[3]));
		}
	}

	void WriteTwitterTokenLibrary()
	{
		string writeString = "";

		foreach (TokenLibraryItem tokenLibrarItem in tokenLibrary)
		{
			writeString += tokenLibrarItem.SavingString() + "\n";
		}

		File.WriteAllText(tolkenLibraryPath, writeString);
	}

	public void EnterPin()
	{
		StartCoroutine(TwitterOther.API.GetAccessToken(CONSUMER_KEY, CONSUMER_SECRET, m_RequestTokenResponse.Token, pinInputField.text,
			   new TwitterOther.AccessTokenCallback(this.OnAccessTokenCallback)));
	}

	public void PostTweet(string postText, int twitterName)
	{
		TwitterOther.AccessTokenResponse requestTokenAccess;
		requestTokenAccess = tokenLibrary[twitterName].AccessTokenResponse();

		StartCoroutine(TwitterOther.API.PostTweet(postText, CONSUMER_KEY, CONSUMER_SECRET, requestTokenAccess, new TwitterOther.PostTweetCallback(this.OnPostTweet)));
	}

	public void RegisterAccount()
	{
		StartCoroutine(TwitterOther.API.GetRequestToken(CONSUMER_KEY, CONSUMER_SECRET,new TwitterOther.RequestTokenCallback(this.OnRequestTokenCallback)));
	}

    void OnRequestTokenCallback(bool success, TwitterOther.RequestTokenResponse response)
    {
        if (success)
        {
            string log = "OnRequestTokenCallback - succeeded";
            log += "\n    Token : " + response.Token;
            log += "\n    TokenSecret : " + response.TokenSecret;
            Debug.Log(log);

            m_RequestTokenResponse = response;

			TwitterOther.API.OpenAuthorizationPage(response.Token);
        }
        else
        {
            print("OnRequestTokenCallback - failed.");
        }
    }

    void OnAccessTokenCallback(bool success, TwitterOther.AccessTokenResponse response)
    {
        if (success)
        {
			AddOrUpdateToken(response);

			WriteTwitterTokenLibrary();
		}
        else
        {
            print("OnAccessTokenCallback - failed.");
        }
    }

	void AddOrUpdateToken(TwitterOther.AccessTokenResponse response)
	{
		foreach (TokenLibraryItem tokenLibraryItem in tokenLibrary)
		{
			if (tokenLibraryItem.ScreenName == response.ScreenName)
			{
				tokenLibraryItem.UpdateDetails(response.UserId, response.Token, response.TokenSecret);
				return;
			}
		}

		tokenLibrary.Add(new TokenLibraryItem(response.UserId, response.ScreenName, response.Token, response.TokenSecret));
	}

    void OnPostTweet(bool success)
    {
        print("OnPostTweet - " + (success ? "succedded." : "failed."));
    }

    void OnGetHashtag(bool success)
    {
        print("OnGetHashtag - " + (success ? "SUCCESS" : "FAIL"));
    }

    public void StartGetHashtag(string hashtag,int hastagNum)
    {
        string text = hashtag;

        if( string.IsNullOrEmpty(text) )
        {
            Debug.LogWarning("StartGetHashtag: no hashtag.");
            return;
        }

        int num = hastagNum;

        if( num < 1 )
        {
            Debug.LogWarning("StartGetHashtag: how many to get?");
            return;
        }

        StartCoroutine(TwitterOther.API.GetHashtag(hashtag, num, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
                       new TwitterOther.GetTimelineCallback(this.OnGetHashtag)));
    }
}



