﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TwitterFunctions : MonoBehaviour
{
	//Singleton to allow easy access to this script instance
	public static TwitterFunctions instance;

	//[Header("UI references")]
	public InputField QueryInput;
	//public Slider tweetNumberSlider, tweetNumberSliderQuery;
	public Text UItoken;

	//[Header("Data retrieved")]
	//public Twitter.TwitterUser user;
	//public string[] IDs;
	public Twitter.Tweet[] tweets;

	[Header("API Access variables")]
	public string consumerKey;
	public string consumerSecret, AccessToken;

	private DatabaseConnect databaseConnectManager;
	public GameController gameController;
	public int newPlayerAccountsPerScan = 20;

	private LogManager logManager;

	void Awake ()
	{
		databaseConnectManager = GetComponent<DatabaseConnect>();
		logManager = GetComponent<LogManager>();
		instance = this;
		GetAccessToken();
		GetCharacters();
	}

	public void GetAccessToken ()
	{
		if (!string.IsNullOrEmpty(consumerKey) && !string.IsNullOrEmpty(consumerSecret))
			AccessToken = Twitter.API.GetTwitterAccessToken(consumerKey,consumerSecret);
		else {
			if (!string.IsNullOrEmpty(consumerSecret))
				Debug.Log("Missing Consumer secret");
			if (!string.IsNullOrEmpty(consumerKey))
				Debug.Log("Missing Consumer key");
		}
	}

	void Update ()
	{
		UItoken.color = (string.IsNullOrEmpty(AccessToken) ? Color.red : Color.green);
		UItoken.text = (string.IsNullOrEmpty(AccessToken) ? "Access Token missing" : "Access Token acquired");
		//tweetNumberText.text = "" + tweetNumberSlider.value;
		//tweetNumberTextQuery.text = "" + tweetNumberSliderQuery.value;
		//Sometimes the instance value can be reset while editing scripts whilst in play mode from the editor
		if (!instance)
			instance = this;		
	}

	//public void GetProfile ()
	//{
	//	if (!string.IsNullOrEmpty(AccessToken)) {
	//		user = Twitter.API.GetProfileInfo(usernameInput.text,AccessToken,false);

	//		if (user != null) {
	//			Texture2D ava = Twitter.helperFunctions.GetTextureFromImageURL(user.profile_image_url);

	//			UserAvatar.sprite = Sprite.Create(
	//				ava,
	//				new Rect (0, 0, ava.width, ava.height),Vector2.one / 2
	//			);
	//		}
	//		else
	//			Debug.Log("User not found");
	//	}
	//	else
	//		Debug.Log("No access token :(");
	//}

	//public void GetTweetsFromUserTimeline ()
	//{
	//	if (!string.IsNullOrEmpty(AccessToken))
	//		tweets = Twitter.API.GetUserTimeline(usernameInput.text,(int)tweetNumberSlider.value,AccessToken);
	//	else
	//		Debug.Log("No access token :(");
	//}

	//public void GetFollowerIDs ()
	//{
	//	if (!string.IsNullOrEmpty(AccessToken)) {
	//		IDs = Twitter.API.GetIDs(usernameInput.text,Twitter.API.IDType.followers,AccessToken,500);
	//		if (IDs != null)
	//			Debug.Log(IDs.Length + " IDs collected from " + usernameInput.text + "'s followers");
	//		else
	//			Debug.Log("No ID's found");
	//	}
	//	else
	//		Debug.Log("No access token :(");
	//}

	//public void GetFriendIDs ()
	//{
	//	if (!string.IsNullOrEmpty(AccessToken)) {
	//		IDs = Twitter.API.GetIDs(usernameInput.text,Twitter.API.IDType.friends,AccessToken);
	//		if (IDs != null)
	//			Debug.Log(IDs.Length + " IDs collected from " + usernameInput.text + "'s friends");
	//		else
	//			Debug.Log("No ID's found");
	//	}
	//	else
	//		Debug.Log("No access token :(");
	//}

	//public void GetRetweeters ()
	//{
	//	if (!string.IsNullOrEmpty(AccessToken)) {
	//		//For sake of example
	//		string tweetID = "790303937794416641";

	//		IDs = Twitter.API.GetIDs(tweetID,Twitter.API.IDType.retweeters,AccessToken);
	//		if (IDs != null)
	//			Debug.Log(IDs.Length + " retweeter IDs collected from " + tweetID);
	//		else
	//			Debug.Log("No ID's found");
	//	}
	//	else
	//		logManager.AddLog("No access token", true);
	//}

	public void GetCharacters()
	{
		databaseConnectManager.GetCharacters();
	}

	public void GetLastPlayerID(Action doNext)
	{
		if (doNext != null)
		{
			databaseConnectManager.GetLatestPlayersID(doNext);
		}
	}

	public void TruncatePlayers()
	{
		databaseConnectManager.TrauncatePlayers();
	}

	public void SearchTwitterForNewAccounts()
	{
		databaseConnectManager.GetLatestPlayersID(SearchTwitterPostPointerGet);
	}

	public void SearchTwitterPostPointerGet ()
	{
		//Check if we have an access token
		if (string.IsNullOrEmpty(AccessToken)) {
			logManager.AddLog("No access token",true);
			return;
		}

		tweets = Twitter.API.SearchForTweets(QueryInput.text,AccessToken, newPlayerAccountsPerScan, Twitter.API.SearchResultType.recent);

		if (tweets == null)
		{
			logManager.AddLog("No tweets found");

			return;
		}

		logManager.AddLog(tweets.Length + " tweets found");

		List<TwitterAccount> twitterAccounts = new List<TwitterAccount>();
		long lastTwitterID = 0;
		foreach (Twitter.Tweet tweet in tweets)
		{
			if (TimeStampInt(tweet) <= databaseConnectManager.LatestNewPlayerTimestamp) {
				continue;
			}
			if (TimeStampInt(tweet) > lastTwitterID)
			{
				lastTwitterID = TimeStampInt(tweet);
			}
			long id = long.Parse(tweet.user.id);
			string screenName =	tweet.user.screen_name;
			string name = tweet.user.name;
			string description = tweet.user.description;
			string imageURL = tweet.user.profile_image_url.Replace("normal", "400x400");
			TwitterAccount twitterAccount = new TwitterAccount(id,screenName,name,description,imageURL);
			twitterAccounts.Add(twitterAccount);
		}

		if (twitterAccounts.Count == 0)
		{
			logManager.AddLog("No new accounts found");
		}
		else
		{
			logManager.AddLog(twitterAccounts.Count.ToString() + " new accounts found");
			
			foreach (TwitterAccount twitterAccount in twitterAccounts)
			{
				databaseConnectManager.AddPlayer(twitterAccount);
				FindObjectOfType<NameList>().AddPlayer(twitterAccount);
			}

			databaseConnectManager.UpdatedLatestSearchedTweet(lastTwitterID);
		}
	}

	long TimeStampInt(Twitter.Tweet tweet) {

		string formattedDateTime = "";

		formattedDateTime += tweet.FormatedDateTime.Year.ToString();

		string month = "~~~";
		switch (tweet.FormatedDateTime.Month)
		{
			case "Jan":
				month = "01";
				break;
			case "Feb":
				month = "02";
				break;
			case "Mar":
				month = "03";
				break;
			case "Apr":
				month = "04";
				break;
			case "May":
				month = "05";
				break;
			case "Jun":
				month = "06";
				break;
			case "Jul":
				month = "07";
				break;
			case "Aug":
				month = "08";
				break;
			case "Sep":
				month = "09";
				break;
			case "Oct":
				month = "10";
				break;
			case "Nov":
				month = "11";
				break;
			case "Dec":
				month = "12";
				break;
			default:
				break;
		}
		formattedDateTime += month;
		formattedDateTime += tweet.FormatedDateTime.Day.ToString("D2");
		formattedDateTime += tweet.FormatedDateTime.Hour.ToString("D2");
		formattedDateTime += tweet.FormatedDateTime.Minute.ToString("D2");
		formattedDateTime += tweet.FormatedDateTime.Second.ToString("D2");
		//Debug.Log(formattedDateTime);
		return long.Parse(formattedDateTime);
	} 
}

#if UNITY_EDITOR
[CustomEditor(typeof(TwitterFunctions))]
public class PrototypeInspectorButtons : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();
		TwitterFunctions t = (TwitterFunctions)target;
		if (GUILayout.Button("Register Twitter App")) {
			Application.OpenURL("http://apps.twitter.com");
		}

		if (GUILayout.Button("Open REST API documentation")) {
			Application.OpenURL("http://dev.twitter.com/rest/reference");
		}

		if (GUILayout.Button("Get access token")) {
			t.GetAccessToken();
		}
	}
}
#endif
