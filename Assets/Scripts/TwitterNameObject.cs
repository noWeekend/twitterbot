﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwitterNameObject : MonoBehaviour {

	public TwitterAccount twitterAccount;
	public Text twitterName;

	public void Initilise(TwitterAccount ta)
	{
		twitterAccount = ta;
		twitterName.text = "@"+ twitterAccount.ScreenName;

	}

	public void OnClick()
	{
		FindObjectOfType<UserInfo>().DisplayData(twitterAccount);
	}
}
