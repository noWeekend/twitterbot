﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameList : MonoBehaviour {

	public Transform playerList;
	public GameObject playerNamePrefab;

	public void AddPlayer(TwitterAccount twitterAccount)
	{
		GameObject GO =Instantiate(playerNamePrefab, playerList) as GameObject;
		GO.GetComponent<TwitterNameObject>().Initilise(twitterAccount);
	}
}
