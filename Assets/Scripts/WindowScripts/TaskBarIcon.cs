﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskBarIcon : MonoBehaviour {

	public Window window;

	public void SelectMe()
	{
		window.SelectMe();
	}
}
