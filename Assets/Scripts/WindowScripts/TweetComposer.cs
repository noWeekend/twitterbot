﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TweetComposer : MonoBehaviour {

	public Dropdown characterDropdown;
	public InputField textInputField;
	public InputField dayInputField;
	public InputField hourInputField;
	public InputField minuteInputField;
	public Button scheduleButton;
	public Button postNowButton;
	public Text infoText;
	public GameController gameController;

	// Use this for initialization
	void Start () {
		gameController = FindObjectOfType<GameController>();
		postNowButton.enabled = false;
		StartCoroutine(LateStart(5));
		characterDropdown.ClearOptions();
	}

	IEnumerator LateStart(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);

		characterDropdown.AddOptions(gameController.GetFullCharacterList());
	}

	bool CheckData(bool fullCheck)
	{
		if (string.IsNullOrEmpty(textInputField.text))
		{
			Feedback("No Tweet Entered");
			return false;
		}

		if (!fullCheck)
		{
			return true;
		}

		if (!TestNumber(dayInputField.text,1,3))
		{
			Feedback("Day must be 1,2 or 3");
			return false;
		}

		if (!TestNumber(hourInputField.text, 0, 23))
		{
			Feedback("Hour must be between 0 and 23");
			return false;
		}

		if(!TestNumber(minuteInputField.text, 0, 59))
		{
			Feedback("Hour must be between 0 and 59");
			return false;
		}

		return true;
	}

	public void PostTweetNow()
	{
		if (!CheckData(false))
		{
			return;
		}

		string timeString = System.DateTime.Now.ToString("HHMM");
		int currentDay = 1; //TODO FIX MEE!!

		PostTweet(currentDay, timeString);
	}

	public void PostTweetLater()
	{
		if (!CheckData(true))
		{
			return;
		}

		string timeString = "";
		if (hourInputField.text.Length == 1)
		{

			timeString += "0";
		}
		timeString += hourInputField.text;

		if (minuteInputField.text.Length == 1)
		{
			timeString += "0";
		}
		timeString += minuteInputField.text;

		int day = int.Parse(dayInputField.text);

		PostTweet(day,timeString);
	}

	void ClearWindow()
	{
		textInputField.text = "";
		dayInputField.text = "";
		hourInputField.text = "";
		minuteInputField.text = "";
	}

	void PostTweet(int day, string timeString)
	{
		gameController.databaseConnect.AddTweet(characterDropdown.value, textInputField.text, day, timeString);

		Feedback("Post Scheduled",true);
		ClearWindow();
	}

	void Feedback(string feedback, bool isGood = false)
	{
		if (isGood)
		{
			infoText.color = Color.green;
		}
		else
		{
			infoText.color = Color.red;
		}

		infoText.text = feedback;
	}

	bool TestNumber(string input, int min, int max) {
		if (string.IsNullOrEmpty(input))
		{
			return false;
		}

		int parsedInt = int.Parse(input);

		if (parsedInt <= max && parsedInt >= min)
		{
			return true;
		}

		return false;
	}
}
