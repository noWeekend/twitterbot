﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInfo : MonoBehaviour {

	TwitterAccount twitterAccount;

	public Text titterName;
	public Text ActualName;
	public Text StartDate;
	public Toggle isPlayingToggle;
	public InputField notes;
	public Image AvatarImage;

	public void DisplayData(TwitterAccount ta)
	{
		twitterAccount = ta;
		Debug.Log(twitterAccount.ScreenName);
		UpdateDetails();
	}

	void UpdateDetails()
	{
		titterName.text = "@" + twitterAccount.ScreenName;
		ActualName.text = twitterAccount.Name;

		ActualName.text = twitterAccount.Name;

		Texture2D ava = Twitter.helperFunctions.GetTextureFromImageURL(twitterAccount.ImageUrl);

		AvatarImage.sprite = Sprite.Create(
			ava,
			new Rect(0, 0, ava.width, ava.height), Vector2.one / 2
		);
	}

	public void OpenURL()
	{
		Application.OpenURL("https://twitter.com/" + twitterAccount.ScreenName);
	}
}
