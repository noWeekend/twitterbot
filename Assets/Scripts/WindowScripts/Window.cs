﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour {


	private Vector3 dragOffset;
	private float lastSelected = 0;
	private CanvasGroup canvasGroup;
    private bool isHidden = true;
    private float lastClickTime = 0;
    private float doubleClickTimeMax = 0.3f;

	protected virtual void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup>();
	}

	public virtual void Seleted()
	{
		if (Time.time - lastSelected > 3)
		{
			SelectMe();
			lastSelected = Time.time;
		}
		else
		{
			Hide();
			lastSelected = 0;
		}
	}

	public virtual void Hide() {
        transform.position = new Vector3(-100, -100);
        isHidden = false;
    }

	public void BeginDrag()
	{
		dragOffset = Input.mousePosition - transform.position;
	}

	public void Drag()
	{
		transform.position = Input.mousePosition - dragOffset;
	}

	public virtual void Restore()
	{
        Rect windowRect = GetComponent<RectTransform>().rect;
        Vector3 restorePosition = new Vector3(windowRect.width/4, windowRect.height / 4);

        transform.localPosition = restorePosition;
    }

	public virtual void SelectMe()
	{
        if(Time.time - lastClickTime < doubleClickTimeMax)
        {
            Restore();
        }

        lastClickTime = Time.time;

        Vector2 size = Vector2.Scale(transform.GetComponent<RectTransform>().rect.size, transform.lossyScale);
        Rect windowRect = new Rect((Vector2)transform.position - (size * 0.5f), size);

        Rect screenRect = Screen.safeArea;

        if (!windowRect.Overlaps(screenRect) || !isHidden)
        {
            Debug.Log("nope");
            Restore();
        }

		transform.SetAsLastSibling();
        isHidden = true;
    }
}
