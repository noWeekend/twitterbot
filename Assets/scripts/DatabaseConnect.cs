﻿using UnityEngine;
using System.Collections;
using System;

public class DatabaseConnect:MonoBehaviour
{
	//Private key that is used for sending information to webserver so that it knows that it is from us.
    private string secretKey = "foodfight"; 

	private string listPlayersPage = "http://www.no-weekend.com/playwithyourfood/displayplayers.php";

	//The timestamp of the last player we signed up
	public long LatestNewPlayerTimestamp = 0;

	private LogManager logManager;
	private GameController gameController;

	void Awake()
	{
		logManager = GetComponent<LogManager>();
		gameController = GetComponent<GameController>();
	}

#region Add Player
	private string addPlayerPage = "http://www.no-weekend.com/playwithyourfood/addplayer.php/?";

	//Add a new player to the game
	public void AddPlayer(TwitterAccount twitterAccount)
	{
		StartCoroutine(AddPlayer(twitterAccount.Id, twitterAccount.ScreenName,twitterAccount.Name,twitterAccount.Description,twitterAccount.ImageUrl));
	}

    IEnumerator AddPlayer(long id,string screenName, string name,string description,string imageURL)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.
        string hash = Md5Sum(id + screenName + name + description + imageURL + secretKey);

        string post_url = addPlayerPage + 
			"id=" + id + 
			"&twittername=" + WWW.EscapeURL(screenName) +
			"&name=" + WWW.EscapeURL(name) +
			"&description=" + WWW.EscapeURL(description) +
			"&imageURL=" + WWW.EscapeURL(imageURL) +
			"&hash=" + hash;

		WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done

		if (hs_post.error != null)
		{
			logManager.AddLog("There was an error adding the player: " + hs_post.error);
		}
		else
		{
			logManager.AddLog("Added player @" + screenName);
			//Debug.Log("PlayerAdded");
		}
    }

	#endregion

#region Add Tweet
	private string addTweetPage = "http://www.no-weekend.com/playwithyourfood/addtweet.php/?";

	//Add a new player to the game
	public void AddTweet(int account, string tweet, int day, string time)
	{
		StartCoroutine(AddTweetCoroutine(account,tweet,day,time));
	}

	IEnumerator AddTweetCoroutine(int account, string tweet, int day, string time)
	{
		//This connects to a server side php script that will add the name and score to a MySQL DB.
		// Supply it with a string representing the players name and the players score.
		string hash = Md5Sum(account + tweet + day + time + secretKey);

		string post_url = addTweetPage +
			"account=" + account +
			"&tweet=" + WWW.EscapeURL(tweet) +
			"&day=" + day +
			"&time=" + WWW.EscapeURL(time) +
			"&hash=" + hash;

		Debug.Log(post_url);

		WWW hs_post = new WWW(post_url);
		yield return hs_post; // Wait until the download is done

		if (hs_post.error != null)
		{
			logManager.AddLog("There was an error adding the tweet: " + hs_post.error);
		}
		else
		{
			logManager.AddLog(hs_post.text);
		}
	}

#endregion

	#region Get Latest Player's ID
	private string latestPlayersTweetIDPage = "http://www.no-weekend.com/playwithyourfood/latestplayerstweetid.php";

	public void GetLatestPlayersID(Action doNextAction)
	{
		Debug.Log(doNextAction);
		StartCoroutine(GetLatestPlayersIDCoroutine(doNextAction));
	}


	IEnumerator GetLatestPlayersIDCoroutine(Action doNextAction)
    {
        WWW hs_get = new WWW(latestPlayersTweetIDPage);
        yield return hs_get;

        if (hs_get.error != null)
        {
			logManager.AddLog("There was an error getting the latest players timestamp: " + hs_get.error);
		}
        else
        {
			LatestNewPlayerTimestamp = long.Parse(hs_get.text);
			logManager.AddLog("Latest players timestap: " + LatestNewPlayerTimestamp.ToString());
			doNextAction.Invoke();
        }
    }

	#endregion

	#region Get Characters
	private string charactersPage = "http://www.no-weekend.com/playwithyourfood/characters.php";

	public void GetCharacters()
	{
		StartCoroutine(GetCharactersCoroutine());
	}


	IEnumerator GetCharactersCoroutine()
	{
		WWW hs_get = new WWW(charactersPage);
		yield return hs_get;

		if (hs_get.error != null)
		{
			logManager.AddLog("There was an error getting the characterList: " + hs_get.error);
		}
		else
		{
			logManager.AddLog("Got List of Characters");
			gameController.SetCharacters(hs_get.text);
		}
	}

	#endregion

	#region Truncate Player table
	private string truncatePlayersPage = "http://www.no-weekend.com/playwithyourfood/truncateplayers.php/?";

	public void TrauncatePlayers()
	{
		StartCoroutine(TruncatePlayersCoroutine());
	}

	IEnumerator TruncatePlayersCoroutine()
	{
		string hash = Md5Sum(secretKey);
		string post_url = truncatePlayersPage + "&hash=" + hash;

		WWW hs_post = new WWW(post_url);
		yield return hs_post; // Wait until the download is done

		Debug.Log(post_url);

		if (hs_post.error != null)
		{
			logManager.AddLog("There was an error truncateing the players: " + hs_post.error);
		}
		else
		{
			logManager.AddLog("Players Truncated");
		}
	}

	#endregion

#region Updated Latest Searched Tweet Timestamp
	private string updatedLatestSeachedTweetIDPage = "http://www.no-weekend.com/playwithyourfood/updatelatestsearchedtweetid.php/?";

	public void UpdatedLatestSearchedTweet(long timeStamp)
	{
		StartCoroutine(UpdatedLatestSearchedTweetCoroutine(timeStamp));
	}

	IEnumerator UpdatedLatestSearchedTweetCoroutine(long timeStamp)
	{
		string hash = Md5Sum(timeStamp + secretKey);
		string post_url = updatedLatestSeachedTweetIDPage + "&tweetid=" + timeStamp + "&hash=" + hash;

		WWW hs_post = new WWW(post_url);
		yield return hs_post; // Wait until the download is done

		if (hs_post.error != null)
		{
			logManager.AddLog("There was an error adding the last tweetID: " + hs_post.error);
		}
		else
		{
			logManager.AddLog("Last Tweet ID updated: " + timeStamp);

			LatestNewPlayerTimestamp = timeStamp;
		}
	}

	#endregion

#region helper functions
	public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

#endregion

}

