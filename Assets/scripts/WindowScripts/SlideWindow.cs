﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideWindow : Window {

    Vector3 originalPosition;
    Vector3 hiddenPosition;

    bool isSlideHidden = false;

    protected override void Awake()
    {
        base.Awake();
        originalPosition = transform.position;
        Rect windowRect = GetComponent<RectTransform>().rect;

    }

    public override void Restore()
    {
        
    }

    public override void Hide()
    {
        GetComponent<Animator>().SetTrigger("HideMe");
    }

    public override void SelectMe()
    {
        PressHideButton();
    }

    void ActualSelect()
    {
        Restore();
        GetComponent<Animator>().SetTrigger("ShowMe");
        transform.SetAsLastSibling();
    }

    public void PressHideButton()
    {
        if (isSlideHidden)
        {
            ActualSelect();
            isSlideHidden = false;
            
        }
        else
        {
            Hide();
            isSlideHidden = true;
            
        }
    }
}
